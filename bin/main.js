/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/***********************************************************************************************************************************************
 MBWB - SCRIPTS - EVENTS - CAPTURE EVENTS
 ***********************************************************************************************************************************************
 * @description
 */

/**
 * Temporary array to hold events; flushed every time SQS is invoked.
 */
var MBWB__EVENT_KEY = 'MBWB__event_queue';
var MBWB__q = JSON.parse(window.localStorage.getItem(MBWB__EVENT_KEY)) || [];
/**
 * Make AWS/SQS available.
 */

window.MBWB__sqs = null;
window.MBWB__sqsQueueUrl = null;
/**
 * Default function for checking online status
 */

window.MBWB__onlineStatusHandler = function () {
  return new Promise(function (resolve, reject) {
    if (window.navigator.onLine) {
      resolve();
    } else {
      reject();
    }
  });
};
/**
 * Debouncer
 */


var debounce = function debounce(func, delay, callback) {
  var inDebounce;
  var count = delay;
  return function () {
    var context = this;
    var args = arguments;
    count = delay;
    clearInterval(inDebounce);
    inDebounce = setInterval(function () {
      func.call(context, args);
      clearInterval(inDebounce);
      callback.call(context, args);
    }, count);
  };
};
/**
 * Event handler
 */


function MBWB__eventHandler() {
  // write to local storage
  console.log('writing q', MBWB__q);
  window.localStorage.setItem(MBWB__EVENT_KEY, JSON.stringify(MBWB__q));
}
/**
 * Send the queue of messages.
 */


function MBWB__sendSQS() {
  // Check online status & continue if the device is online.
  MBWB__onlineStatusHandler().then(function (status) {
    // Add locally-stored events to a processing array.
    var q = JSON.parse(window.localStorage.getItem(MBWB__EVENT_KEY));
    MBWB__q = []; // Clear local storage.

    window.localStorage.setItem(MBWB__EVENT_KEY, JSON.stringify([])); // Runner.

    for (var i = 0; i <= q.length; i++) {
      var message = JSON.stringify(q[i]);

      if (!message) {
        continue;
      }

      window.MBWB__sqs.sendMessage({
        MessageBody: message,
        QueueUrl: window.MBWB__sqsQueueUrl
      }, function (err) {
        if (err) {
          throw new Error(err);
        }
      });
    }
  }, function () {
    console.warn('MBWB EVENTS: offline. Event stored locally');
  });
}
/**
 * Formats the event to be more readable
 */


function MBWB__formatEvent(e, metadata) {
  var date = new Date();
  return Object.assign({}, {
    date: date.toISOString(),
    element: {
      type: e.target.localName || e.target.tagName,
      id: e.target.id || null,
      class: e.target.className,
      name: e.target.name || null,
      parent: {
        type: e.target.parentElement.localName || e.target.parentElement.tagName,
        id: e.target.parentElement.id || null,
        class: e.target.parentElement.className,
        name: e.target.parentElement.name || null
      }
    },
    href: window.location.href
  }, {
    data: metadata || {}
  });
}
/**
 * Alias the debounced event handler function.
 */


var debouncer = debounce(MBWB__eventHandler, 500, MBWB__sendSQS);
/**
 * Attach the event handlers.
 */

window.mbwbEvents = function (creds, config, metadata, status) {
  // Make sure we have proper credentials and configuration.
  if (!creds) {
    throw new Error('Please provide your AWS credentials.');
  } else {
    if (!creds.AWSAccessKeyId || !creds.AWSSecretAccessKey) {
      throw new Error('Please provide an AWS Access Key ID and your AWS Secret Access Key.');
    }
  }

  if (!config) {
    throw new Error('Please provide an SQS config object');
  } else {
    if (!config.SQSEndpoint || !config.AWSRegion) {
      throw new Error('Please provide an SQS Queue URL and an AWS Region.');
    }
  } // Ensure status handler is a function that returns a promise


  if (status && typeof status === 'function') {
    window.MBWB__onlineStatusHandler = status; // var statusTest = status();
    // if(!(statusTest instanceof Promise)) {
    //   throw new Error('MBWB Events - the online status handler needs to be a promise');
    // } else {
    //   window.MBWB__onlineStatusHandler = status;
    // }
  } else {
    throw new Error('MBWB Events - the online status handler needs to be a function.');
  } // Configure SQS


  window.MBWB__sqs = new AWS.SQS({
    apiVersion: '2012-11-05',
    accessKeyId: creds.AWSAccessKeyId,
    secretAccessKey: creds.AWSSecretAccessKey,
    region: config.AWSRegion
  }); // Alias the queue URL for other functions.

  window.MBWB__sqsQueueUrl = config.SQSEndpoint; // Set up event listeners.

  window.addEventListener('click', function (e) {
    var data = metadata;

    if (typeof data === 'function') {
      data = data();

      if (data instanceof Promise) {
        data.then(function (res) {
          // Format the event object to be sent to the queue.
          var event = MBWB__formatEvent(e, res); // Push the event to the queue.

          MBWB__q.push(event); // Run the debouncer.

          debouncer();
        }, function (err) {
          console.log('MBWB EVENTS - error calling metadata function: ', err);
        });
      } else {
        // Format the event object to be sent to the queue.
        var event = MBWB__formatEvent(e, data); // Push the event to the queue.

        MBWB__q.push(event); // Run the debouncer.

        debouncer();
      }
    } else {
      // Format the event object to be sent to the queue.
      var event = MBWB__formatEvent(e, data); // Push the event to the queue.

      MBWB__q.push(event); // Run the debouncer.

      debouncer();
    }
  });
};

/***/ })
/******/ ]);
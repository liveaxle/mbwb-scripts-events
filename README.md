# mbwb-scripts-events

This script captures event data in a host application and sends it to an SQS queue.

## Usage
Include this script and `<script src="https://sdk.amazonaws.com/js/aws-sdk-2.499.0.min.js"></script>` in your application.

Call the `mbwbEvents` function and provide credentials, configuration, and any metadata you would like to capture.

## Installing

`npm install @liveaxle/mbwb-capture`;

## Config
The `mbwbEvents` function takes three objects as arguments: `creds`, `config` and `metadata`.

### Creds

```
{
  AWSAccessKeyId: 'foo',
  AWSSecretAccessKey: 'bar'
}
```

### Config
```
{
  AWSRegion: 'your-aws-region',
  SQSEndpoint: 'your-sqs-endpoint-url'
}
```

### Metadata
The metadata object can be used to attach any extra data you would like to store.

### ES6

### Browser (and Angular 1.x)

`import '@liveaxle/mbwb-capture/bin/main.js'`

or

`<script src="node_modules/@liveaxle/mbwb-capture/bin/main.js"></script>`

Both of these reference options will give you:

`window.mbwbEvents`

## Publishing

After making a change do the following to make your code available:
1. `npm install`
2. `npm login` follow the prompt and log in to npm
3. Make sure your repo is up to date with any remote changes or else later commands will break
4. `npm run build`
5. `npm version patch`
6. Commit the things
7. `git push origin --tags`
8. `npm publish`

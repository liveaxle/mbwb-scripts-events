'use strict';

/***********************************************************************************************************************************************
 * MBWB - CAPTURE
 ***********************************************************************************************************************************************
 * @description
 */
const path = require('path');

module.exports = (env) => {
  return {
    entry: path.join(__dirname, 'capture.js'),
    output: {
      path: path.join(__dirname, 'bin'),
      filename: 'main.js'
    },
    mode: 'production',
    module: {
      rules: [
        {test: /\.js/, use: 'babel-loader', exclude: /node_modules/}
      ]
    },
    node: {
      fs: "empty",
      dns: "empty",
      net: "empty",
      __dirname: true
    },
    resolve: {
      alias: {
        '~': path.join(__dirname)
      }
    },
    optimization: {
      minimize: false
    }
  }
}
